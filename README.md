# ensime-lsp-clients
Collection of clients for ENSIME LSP implementation

Currently supported editors:
- vs-code (`ensime-lsp-vscode` folder)
- atom (`ensime-lsp-atom` folder)


